# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [2.5.0] - 2020-06-23
### Added
- Added concept association to staging xsd

## [2.4.0] - 2020-04-29
### Added
- Json import schema
- restrict languages in import schemas
### Changed
- apply google java code style

## [2.3.0] - 2019-09-05
### Added
- TBD ValidationType to Staging schema

## [2.2.2] - 2018-11-08
### Added
- "Import" Element to group multiple staged elements
- Staged Groups

## [2.2.1] - 2018-07-25
### Changed 
- import to namespace no longer mandatory for staged elements

## [2.2.0] - 2018-07-20
### Added 
- staged elements

## [2.1.0] - 2016-09-02
### Added 
- status for scoped identifiers
