/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Affero General Public License as published by the Free
 * Software Foundation; either version 3 of the License, or (at your option) any
 * later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it
 * with Jersey (https://jersey.java.net) (or a modified version of that
 * library), containing parts covered by the terms of the General Public
 * License, version 2.0, the licensors of this Program grant you additional
 * permission to convey the resulting work.
 */

package de.samply.mdr.json;

import com.google.gson.JsonObject;
import de.samply.mdr.xsd.staging.ImportType;
import de.samply.mdr.xsd.staging.ObjectFactory;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;


public class JsonConverter {

  /**
   * convert jsonObject into an ImportType Object.
   *
   * @param jsonObject user input via rest api
   * @return ImportType-Object
   * @throws JAXBException Object cannot bind to ImportType
   * @throws TransformerException Document Source cannot be transformed to a StreamResult
   * @throws ParserConfigurationException thrown from xmlConvert method
   * @throws IOException thrown from xmlConvert method
   */
  public ImportType converter(JsonObject jsonObject)
      throws JAXBException, TransformerException, ParserConfigurationException, IOException {

    JAXBContext jaxbContext = JAXBContext.newInstance(ObjectFactory.class);
    Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
    Document document = xmlConverter(jsonObject);

    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
    Source xmlSource = new DOMSource(document);
    Result outputTarget = new StreamResult(outputStream);
    TransformerFactory.newInstance().newTransformer().transform(xmlSource, outputTarget);
    InputStream is = new ByteArrayInputStream(outputStream.toByteArray());

    return (ImportType) ((JAXBElement) unmarshaller.unmarshal(is)).getValue();
  }

  /**
   * creates an xml Document with given jsonObject.
   *
   * @param jsonObject user input via rest api
   * @return doc
   * @throws ParserConfigurationException DocumentBuilder cannot be created
   * @throws IOException thrown from convert method
   */
  public Document xmlConverter(JsonObject jsonObject)
      throws ParserConfigurationException, IOException {
    DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
    DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
    Document doc = docBuilder.newDocument();

    Element root = doc.createElementNS("http://schema.samply.de/StagedElement", "ns2:import");
    doc.appendChild(convert(doc, jsonObject, root));

    return doc;
  }

  /**
   * converts given jsonObject in Node-Object.
   *
   * @param doc Document to build node
   * @param jsonObject user input via rest api
   * @param parent Node where childes should be appended
   * @return node
   * @throws IOException thrown when jsonObject Format is not recognized
   */
  public Node convert(Document doc, JsonObject jsonObject, Node parent)
      throws IOException {
    for (String key : jsonObject.keySet()) {
      Element node;
      switch (key) {
        case "slot_key":
          node = doc.createElement("key");
          break;
        case "slot_value":
          node = doc.createElement("value");
          break;
        case "validation":
          node = doc.createElement(key);
          Attr newattr = doc.createAttributeNS("http://www.w3.org/2001/XMLSchema-instance", "type");
          newattr.setValue(
              "ns2:" + jsonObject.get(key).getAsJsonObject().get("validationType").getAsString());
          newattr.setPrefix("xsi");
          node.setAttributeNodeNS(newattr);
          break;
        default:
          node = doc.createElement(key);
          break;
      }
      parent.appendChild(node);
      if (jsonObject.get(key).isJsonPrimitive()) {
        node.appendChild(doc.createTextNode(jsonObject.get(key).getAsString()));
      } else if (jsonObject.get(key).isJsonObject()) {
        convert(doc, jsonObject.get(key).getAsJsonObject(), node);
      } else if (jsonObject.get(key).isJsonArray()) {
        for (int i = 0; i < jsonObject.get(key).getAsJsonArray().size(); i++) {
          Node subnode = null;
          if (key.equals("definitions")) {
            subnode = doc.createElement("definition");
          } else if (key.equals("slots")) {
            subnode = doc.createElement("slot");
          } else if (key.equals("conceptAssociations")) {
            subnode = doc.createElement("conceptAssociation");
          }
          if (subnode != null) {
            convert(doc, jsonObject.get(key).getAsJsonArray().get(i).getAsJsonObject(), subnode);
            node.appendChild(subnode);
          } else {
            convert(doc, jsonObject.get(key).getAsJsonArray().get(i).getAsJsonObject(), node);
          }
        }

      } else {
        throw new IOException("Error: " + "JsonObject not recognized!");
      }
    }

    return parent;
  }

}
