
package de.samply.mdr.xsd.staging;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for validationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="validationType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="validationType" type="{http://schema.samply.de/StagedElement}validationTypeEnum"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "validationType", propOrder = {
    "validationType"
})
@XmlSeeAlso({
    TbdValidation.class,
    BooleanValidation.class,
    CalendarValidation.class,
    CatalogValidation.class,
    FloatValidation.class,
    IntegerValidation.class,
    PermittedValuesValidation.class,
    StringValidation.class
})
public abstract class ValidationType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ValidationTypeEnum validationType;

    /**
     * Gets the value of the validationType property.
     * 
     * @return
     *     possible object is
     *     {@link ValidationTypeEnum }
     *     
     */
    public ValidationTypeEnum getValidationType() {
        return validationType;
    }

    /**
     * Sets the value of the validationType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidationTypeEnum }
     *     
     */
    public void setValidationType(ValidationTypeEnum value) {
        this.validationType = value;
    }

}
