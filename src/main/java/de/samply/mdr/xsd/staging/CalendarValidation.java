
package de.samply.mdr.xsd.staging;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for calendarValidation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="calendarValidation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schema.samply.de/StagedElement}validationType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="dateFormat" type="{http://schema.samply.de/StagedElement}dateFormatString"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "calendarValidation", propOrder = {
    "dateFormat"
})
public class CalendarValidation
    extends ValidationType
{

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected DateFormatString dateFormat;

    /**
     * Gets the value of the dateFormat property.
     * 
     * @return
     *     possible object is
     *     {@link DateFormatString }
     *     
     */
    public DateFormatString getDateFormat() {
        return dateFormat;
    }

    /**
     * Sets the value of the dateFormat property.
     * 
     * @param value
     *     allowed object is
     *     {@link DateFormatString }
     *     
     */
    public void setDateFormat(DateFormatString value) {
        this.dateFormat = value;
    }

}
