
package de.samply.mdr.xsd.staging;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the de.samply.mdr.xsd.staging package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Import_QNAME = new QName("http://schema.samply.de/StagedElement", "import");
    private final static QName _StagedElement_QNAME = new QName("http://schema.samply.de/StagedElement", "stagedElement");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: de.samply.mdr.xsd.staging
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ImportType }
     * 
     */
    public ImportType createImportType() {
        return new ImportType();
    }

    /**
     * Create an instance of {@link StagedElementType }
     * 
     */
    public StagedElementType createStagedElementType() {
        return new StagedElementType();
    }

    /**
     * Create an instance of {@link Members }
     * 
     */
    public Members createMembers() {
        return new Members();
    }

    /**
     * Create an instance of {@link Slots }
     * 
     */
    public Slots createSlots() {
        return new Slots();
    }

    /**
     * Create an instance of {@link ConceptAssociationType }
     * 
     */
    public ConceptAssociationType createConceptAssociationType() {
        return new ConceptAssociationType();
    }

    /**
     * Create an instance of {@link ConceptAssociations }
     * 
     */
    public ConceptAssociations createConceptAssociations() {
        return new ConceptAssociations();
    }

    /**
     * Create an instance of {@link Definitions }
     * 
     */
    public Definitions createDefinitions() {
        return new Definitions();
    }

    /**
     * Create an instance of {@link DefinitionType }
     * 
     */
    public DefinitionType createDefinitionType() {
        return new DefinitionType();
    }

    /**
     * Create an instance of {@link SlotType }
     * 
     */
    public SlotType createSlotType() {
        return new SlotType();
    }

    /**
     * Create an instance of {@link TbdValidation }
     * 
     */
    public TbdValidation createTbdValidation() {
        return new TbdValidation();
    }

    /**
     * Create an instance of {@link BooleanValidation }
     * 
     */
    public BooleanValidation createBooleanValidation() {
        return new BooleanValidation();
    }

    /**
     * Create an instance of {@link CalendarValidation }
     * 
     */
    public CalendarValidation createCalendarValidation() {
        return new CalendarValidation();
    }

    /**
     * Create an instance of {@link CatalogValidation }
     * 
     */
    public CatalogValidation createCatalogValidation() {
        return new CatalogValidation();
    }

    /**
     * Create an instance of {@link FloatValidation }
     * 
     */
    public FloatValidation createFloatValidation() {
        return new FloatValidation();
    }

    /**
     * Create an instance of {@link IntegerValidation }
     * 
     */
    public IntegerValidation createIntegerValidation() {
        return new IntegerValidation();
    }

    /**
     * Create an instance of {@link PermittedValuesValidation }
     * 
     */
    public PermittedValuesValidation createPermittedValuesValidation() {
        return new PermittedValuesValidation();
    }

    /**
     * Create an instance of {@link StringValidation }
     * 
     */
    public StringValidation createStringValidation() {
        return new StringValidation();
    }

    /**
     * Create an instance of {@link PermittedValueType }
     * 
     */
    public PermittedValueType createPermittedValueType() {
        return new PermittedValueType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ImportType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link ImportType }{@code >}
     */
    @XmlElementDecl(namespace = "http://schema.samply.de/StagedElement", name = "import")
    public JAXBElement<ImportType> createImport(ImportType value) {
        return new JAXBElement<ImportType>(_Import_QNAME, ImportType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link StagedElementType }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link StagedElementType }{@code >}
     */
    @XmlElementDecl(namespace = "http://schema.samply.de/StagedElement", name = "stagedElement")
    public JAXBElement<StagedElementType> createStagedElement(StagedElementType value) {
        return new JAXBElement<StagedElementType>(_StagedElement_QNAME, StagedElementType.class, null, value);
    }

}
