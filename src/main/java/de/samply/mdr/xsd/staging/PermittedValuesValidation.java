
package de.samply.mdr.xsd.staging;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for permittedValuesValidation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="permittedValuesValidation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schema.samply.de/StagedElement}validationType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="permittedValue" type="{http://schema.samply.de/StagedElement}permittedValueType" maxOccurs="unbounded"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "permittedValuesValidation", propOrder = {
    "permittedValue"
})
public class PermittedValuesValidation
    extends ValidationType
{

    @XmlElement(required = true)
    protected List<PermittedValueType> permittedValue;

    /**
     * Gets the value of the permittedValue property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the permittedValue property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPermittedValue().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link PermittedValueType }
     * 
     * 
     */
    public List<PermittedValueType> getPermittedValue() {
        if (permittedValue == null) {
            permittedValue = new ArrayList<PermittedValueType>();
        }
        return this.permittedValue;
    }

}
