
package de.samply.mdr.xsd.staging;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for dateFormatString.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="dateFormatString"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="yyyy-MM-dd"/&gt;
 *     &lt;enumeration value="yyyy-MM"/&gt;
 *     &lt;enumeration value="dd.MM.yyyy"/&gt;
 *     &lt;enumeration value="MM.yyyy"/&gt;
 *     &lt;enumeration value="HH:mm:ss"/&gt;
 *     &lt;enumeration value="HH:mm"/&gt;
 *     &lt;enumeration value="hh:mm:ss a"/&gt;
 *     &lt;enumeration value="hh:mm a"/&gt;
 *     &lt;enumeration value="yyyy-MM-dd HH:mm:ss"/&gt;
 *     &lt;enumeration value="yyyy-MM-dd HH:mm"/&gt;
 *     &lt;enumeration value="yyyy-MM-dd hh:mm:ss a"/&gt;
 *     &lt;enumeration value="yyyy-MM-dd hh:mm a"/&gt;
 *     &lt;enumeration value="dd.MM.yyyy HH:mm:ss"/&gt;
 *     &lt;enumeration value="dd.MM.yyyy HH:mm"/&gt;
 *     &lt;enumeration value="dd.MM.yyyy hh:mm:ss a"/&gt;
 *     &lt;enumeration value="dd.MM.yyyy hh:mm a"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "dateFormatString")
@XmlEnum
public enum DateFormatString {

    @XmlEnumValue("yyyy-MM-dd")
    YYYY_MM_DD("yyyy-MM-dd"),
    @XmlEnumValue("yyyy-MM")
    YYYY_MM("yyyy-MM"),
    @XmlEnumValue("dd.MM.yyyy")
    DD_MM_YYYY("dd.MM.yyyy"),
    @XmlEnumValue("MM.yyyy")
    MM_YYYY("MM.yyyy"),
    @XmlEnumValue("HH:mm:ss")
    HH_MM_SS("HH:mm:ss"),
    @XmlEnumValue("HH:mm")
    HH_MM("HH:mm"),
    @XmlEnumValue("hh:mm:ss a")
    HH_MM_SS_A("hh:mm:ss a"),
    @XmlEnumValue("hh:mm a")
    HH_MM_A("hh:mm a"),
    @XmlEnumValue("yyyy-MM-dd HH:mm:ss")
    YYYY_MM_DD_HH_MM_SS("yyyy-MM-dd HH:mm:ss"),
    @XmlEnumValue("yyyy-MM-dd HH:mm")
    YYYY_MM_DD_HH_MM("yyyy-MM-dd HH:mm"),
    @XmlEnumValue("yyyy-MM-dd hh:mm:ss a")
    YYYY_MM_DD_HH_MM_SS_A("yyyy-MM-dd hh:mm:ss a"),
    @XmlEnumValue("yyyy-MM-dd hh:mm a")
    YYYY_MM_DD_HH_MM_A("yyyy-MM-dd hh:mm a"),
    @XmlEnumValue("dd.MM.yyyy HH:mm:ss")
    DD_MM_YYYY_HH_MM_SS("dd.MM.yyyy HH:mm:ss"),
    @XmlEnumValue("dd.MM.yyyy HH:mm")
    DD_MM_YYYY_HH_MM("dd.MM.yyyy HH:mm"),
    @XmlEnumValue("dd.MM.yyyy hh:mm:ss a")
    DD_MM_YYYY_HH_MM_SS_A("dd.MM.yyyy hh:mm:ss a"),
    @XmlEnumValue("dd.MM.yyyy hh:mm a")
    DD_MM_YYYY_HH_MM_A("dd.MM.yyyy hh:mm a");
    private final String value;

    DateFormatString(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static DateFormatString fromValue(String v) {
        for (DateFormatString c: DateFormatString.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
