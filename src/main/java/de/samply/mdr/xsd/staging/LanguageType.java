
package de.samply.mdr.xsd.staging;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for languageType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="languageType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="en"/&gt;
 *     &lt;enumeration value="bg"/&gt;
 *     &lt;enumeration value="es"/&gt;
 *     &lt;enumeration value="cz"/&gt;
 *     &lt;enumeration value="da"/&gt;
 *     &lt;enumeration value="de"/&gt;
 *     &lt;enumeration value="et"/&gt;
 *     &lt;enumeration value="el"/&gt;
 *     &lt;enumeration value="fr"/&gt;
 *     &lt;enumeration value="ga"/&gt;
 *     &lt;enumeration value="hr"/&gt;
 *     &lt;enumeration value="it"/&gt;
 *     &lt;enumeration value="lv"/&gt;
 *     &lt;enumeration value="lt"/&gt;
 *     &lt;enumeration value="hu"/&gt;
 *     &lt;enumeration value="mt"/&gt;
 *     &lt;enumeration value="nl"/&gt;
 *     &lt;enumeration value="pl"/&gt;
 *     &lt;enumeration value="pt"/&gt;
 *     &lt;enumeration value="ro"/&gt;
 *     &lt;enumeration value="sk"/&gt;
 *     &lt;enumeration value="sl"/&gt;
 *     &lt;enumeration value="fi"/&gt;
 *     &lt;enumeration value="sv"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "languageType")
@XmlEnum
public enum LanguageType {

    @XmlEnumValue("en")
    EN("en"),
    @XmlEnumValue("bg")
    BG("bg"),
    @XmlEnumValue("es")
    ES("es"),
    @XmlEnumValue("cz")
    CZ("cz"),
    @XmlEnumValue("da")
    DA("da"),
    @XmlEnumValue("de")
    DE("de"),
    @XmlEnumValue("et")
    ET("et"),
    @XmlEnumValue("el")
    EL("el"),
    @XmlEnumValue("fr")
    FR("fr"),
    @XmlEnumValue("ga")
    GA("ga"),
    @XmlEnumValue("hr")
    HR("hr"),
    @XmlEnumValue("it")
    IT("it"),
    @XmlEnumValue("lv")
    LV("lv"),
    @XmlEnumValue("lt")
    LT("lt"),
    @XmlEnumValue("hu")
    HU("hu"),
    @XmlEnumValue("mt")
    MT("mt"),
    @XmlEnumValue("nl")
    NL("nl"),
    @XmlEnumValue("pl")
    PL("pl"),
    @XmlEnumValue("pt")
    PT("pt"),
    @XmlEnumValue("ro")
    RO("ro"),
    @XmlEnumValue("sk")
    SK("sk"),
    @XmlEnumValue("sl")
    SL("sl"),
    @XmlEnumValue("fi")
    FI("fi"),
    @XmlEnumValue("sv")
    SV("sv");
    private final String value;

    LanguageType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static LanguageType fromValue(String v) {
        for (LanguageType c: LanguageType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
