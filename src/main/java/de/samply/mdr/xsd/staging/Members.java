
package de.samply.mdr.xsd.staging;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for members complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="members"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="stagedElement" type="{http://schema.samply.de/StagedElement}stagedElementType" maxOccurs="unbounded" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "members", propOrder = {
    "stagedElement"
})
public class Members {

    protected List<StagedElementType> stagedElement;

    /**
     * Gets the value of the stagedElement property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the stagedElement property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getStagedElement().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link StagedElementType }
     * 
     * 
     */
    public List<StagedElementType> getStagedElement() {
        if (stagedElement == null) {
            stagedElement = new ArrayList<StagedElementType>();
        }
        return this.stagedElement;
    }

}
