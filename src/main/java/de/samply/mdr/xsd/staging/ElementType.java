
package de.samply.mdr.xsd.staging;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for elementType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="elementType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="dataelement"/&gt;
 *     &lt;enumeration value="dataelementgroup"/&gt;
 *     &lt;enumeration value="record"/&gt;
 *     &lt;enumeration value="catalog"/&gt;
 *     &lt;enumeration value="code"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "elementType")
@XmlEnum
public enum ElementType {

    @XmlEnumValue("dataelement")
    DATAELEMENT("dataelement"),
    @XmlEnumValue("dataelementgroup")
    DATAELEMENTGROUP("dataelementgroup"),
    @XmlEnumValue("record")
    RECORD("record"),
    @XmlEnumValue("catalog")
    CATALOG("catalog"),
    @XmlEnumValue("code")
    CODE("code");
    private final String value;

    ElementType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ElementType fromValue(String v) {
        for (ElementType c: ElementType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
