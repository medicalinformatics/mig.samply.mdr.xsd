
package de.samply.mdr.xsd.staging;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for catalogValidation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="catalogValidation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schema.samply.de/StagedElement}validationType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="catalogUrn" type="{http://www.w3.org/2001/XMLSchema}string"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "catalogValidation", propOrder = {
    "catalogUrn"
})
public class CatalogValidation
    extends ValidationType
{

    @XmlElement(required = true)
    protected String catalogUrn;

    /**
     * Gets the value of the catalogUrn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCatalogUrn() {
        return catalogUrn;
    }

    /**
     * Sets the value of the catalogUrn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCatalogUrn(String value) {
        this.catalogUrn = value;
    }

}
