
package de.samply.mdr.xsd.staging;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for stagedElementType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="stagedElementType"&gt;
 *   &lt;complexContent&gt;
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="elementType" type="{http://schema.samply.de/StagedElement}elementType"/&gt;
 *         &lt;element name="definitions" type="{http://schema.samply.de/StagedElement}definitions"/&gt;
 *         &lt;element name="slots" type="{http://schema.samply.de/StagedElement}slots" minOccurs="0"/&gt;
 *         &lt;element name="conceptAssociations" type="{http://schema.samply.de/StagedElement}conceptAssociations" minOccurs="0"/&gt;
 *         &lt;element name="validation" type="{http://schema.samply.de/StagedElement}validationType" minOccurs="0"/&gt;
 *         &lt;element name="members" type="{http://schema.samply.de/StagedElement}members" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/restriction&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "stagedElementType", propOrder = {
    "elementType",
    "definitions",
    "slots",
    "conceptAssociations",
    "validation",
    "members"
})
public class StagedElementType {

    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected ElementType elementType;
    @XmlElement(required = true)
    protected Definitions definitions;
    protected Slots slots;
    protected ConceptAssociations conceptAssociations;
    protected ValidationType validation;
    protected Members members;

    /**
     * Gets the value of the elementType property.
     * 
     * @return
     *     possible object is
     *     {@link ElementType }
     *     
     */
    public ElementType getElementType() {
        return elementType;
    }

    /**
     * Sets the value of the elementType property.
     * 
     * @param value
     *     allowed object is
     *     {@link ElementType }
     *     
     */
    public void setElementType(ElementType value) {
        this.elementType = value;
    }

    /**
     * Gets the value of the definitions property.
     * 
     * @return
     *     possible object is
     *     {@link Definitions }
     *     
     */
    public Definitions getDefinitions() {
        return definitions;
    }

    /**
     * Sets the value of the definitions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Definitions }
     *     
     */
    public void setDefinitions(Definitions value) {
        this.definitions = value;
    }

    /**
     * Gets the value of the slots property.
     * 
     * @return
     *     possible object is
     *     {@link Slots }
     *     
     */
    public Slots getSlots() {
        return slots;
    }

    /**
     * Sets the value of the slots property.
     * 
     * @param value
     *     allowed object is
     *     {@link Slots }
     *     
     */
    public void setSlots(Slots value) {
        this.slots = value;
    }

    /**
     * Gets the value of the conceptAssociations property.
     * 
     * @return
     *     possible object is
     *     {@link ConceptAssociations }
     *     
     */
    public ConceptAssociations getConceptAssociations() {
        return conceptAssociations;
    }

    /**
     * Sets the value of the conceptAssociations property.
     * 
     * @param value
     *     allowed object is
     *     {@link ConceptAssociations }
     *     
     */
    public void setConceptAssociations(ConceptAssociations value) {
        this.conceptAssociations = value;
    }

    /**
     * Gets the value of the validation property.
     * 
     * @return
     *     possible object is
     *     {@link ValidationType }
     *     
     */
    public ValidationType getValidation() {
        return validation;
    }

    /**
     * Sets the value of the validation property.
     * 
     * @param value
     *     allowed object is
     *     {@link ValidationType }
     *     
     */
    public void setValidation(ValidationType value) {
        this.validation = value;
    }

    /**
     * Gets the value of the members property.
     * 
     * @return
     *     possible object is
     *     {@link Members }
     *     
     */
    public Members getMembers() {
        return members;
    }

    /**
     * Sets the value of the members property.
     * 
     * @param value
     *     allowed object is
     *     {@link Members }
     *     
     */
    public void setMembers(Members value) {
        this.members = value;
    }

}
