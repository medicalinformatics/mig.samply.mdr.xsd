
package de.samply.mdr.xsd.staging;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for validationTypeEnum.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="validationTypeEnum"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="booleanValidation"/&gt;
 *     &lt;enumeration value="calendarValidation"/&gt;
 *     &lt;enumeration value="catalogValidation"/&gt;
 *     &lt;enumeration value="floatValidation"/&gt;
 *     &lt;enumeration value="integerValidation"/&gt;
 *     &lt;enumeration value="permittedValuesValidation"/&gt;
 *     &lt;enumeration value="stringValidation"/&gt;
 *     &lt;enumeration value="tbdValidation"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "validationTypeEnum")
@XmlEnum
public enum ValidationTypeEnum {

    @XmlEnumValue("booleanValidation")
    BOOLEAN_VALIDATION("booleanValidation"),
    @XmlEnumValue("calendarValidation")
    CALENDAR_VALIDATION("calendarValidation"),
    @XmlEnumValue("catalogValidation")
    CATALOG_VALIDATION("catalogValidation"),
    @XmlEnumValue("floatValidation")
    FLOAT_VALIDATION("floatValidation"),
    @XmlEnumValue("integerValidation")
    INTEGER_VALIDATION("integerValidation"),
    @XmlEnumValue("permittedValuesValidation")
    PERMITTED_VALUES_VALIDATION("permittedValuesValidation"),
    @XmlEnumValue("stringValidation")
    STRING_VALIDATION("stringValidation"),
    @XmlEnumValue("tbdValidation")
    TBD_VALIDATION("tbdValidation");
    private final String value;

    ValidationTypeEnum(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static ValidationTypeEnum fromValue(String v) {
        for (ValidationTypeEnum c: ValidationTypeEnum.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
