
package de.samply.mdr.xsd.staging;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for floatValidation complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="floatValidation"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schema.samply.de/StagedElement}validationType"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="withinRange" type="{http://www.w3.org/2001/XMLSchema}boolean"/&gt;
 *         &lt;element name="rangeFrom" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="rangeTo" type="{http://www.w3.org/2001/XMLSchema}double" minOccurs="0"/&gt;
 *         &lt;element name="unitOfMeasure" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "floatValidation", propOrder = {
    "withinRange",
    "rangeFrom",
    "rangeTo",
    "unitOfMeasure"
})
public class FloatValidation
    extends ValidationType
{

    protected boolean withinRange;
    protected Double rangeFrom;
    protected Double rangeTo;
    protected String unitOfMeasure;

    /**
     * Gets the value of the withinRange property.
     * 
     */
    public boolean isWithinRange() {
        return withinRange;
    }

    /**
     * Sets the value of the withinRange property.
     * 
     */
    public void setWithinRange(boolean value) {
        this.withinRange = value;
    }

    /**
     * Gets the value of the rangeFrom property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getRangeFrom() {
        return rangeFrom;
    }

    /**
     * Sets the value of the rangeFrom property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setRangeFrom(Double value) {
        this.rangeFrom = value;
    }

    /**
     * Gets the value of the rangeTo property.
     * 
     * @return
     *     possible object is
     *     {@link Double }
     *     
     */
    public Double getRangeTo() {
        return rangeTo;
    }

    /**
     * Sets the value of the rangeTo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Double }
     *     
     */
    public void setRangeTo(Double value) {
        this.rangeTo = value;
    }

    /**
     * Gets the value of the unitOfMeasure property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUnitOfMeasure() {
        return unitOfMeasure;
    }

    /**
     * Sets the value of the unitOfMeasure property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUnitOfMeasure(String value) {
        this.unitOfMeasure = value;
    }

}
