
package de.samply.mdr.xsd.staging;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlEnumValue;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for relationType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="relationType"&gt;
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string"&gt;
 *     &lt;enumeration value="undefined"/&gt;
 *     &lt;enumeration value="equal"/&gt;
 *     &lt;enumeration value="equivalent"/&gt;
 *     &lt;enumeration value="wider"/&gt;
 *     &lt;enumeration value="subsumes"/&gt;
 *     &lt;enumeration value="narrower"/&gt;
 *     &lt;enumeration value="specializes"/&gt;
 *     &lt;enumeration value="inexact"/&gt;
 *   &lt;/restriction&gt;
 * &lt;/simpleType&gt;
 * </pre>
 * 
 */
@XmlType(name = "relationType")
@XmlEnum
public enum RelationType {

    @XmlEnumValue("undefined")
    UNDEFINED("undefined"),
    @XmlEnumValue("equal")
    EQUAL("equal"),
    @XmlEnumValue("equivalent")
    EQUIVALENT("equivalent"),
    @XmlEnumValue("wider")
    WIDER("wider"),
    @XmlEnumValue("subsumes")
    SUBSUMES("subsumes"),
    @XmlEnumValue("narrower")
    NARROWER("narrower"),
    @XmlEnumValue("specializes")
    SPECIALIZES("specializes"),
    @XmlEnumValue("inexact")
    INEXACT("inexact");
    private final String value;

    RelationType(String v) {
        value = v;
    }

    public String value() {
        return value;
    }

    public static RelationType fromValue(String v) {
        for (RelationType c: RelationType.values()) {
            if (c.value.equals(v)) {
                return c;
            }
        }
        throw new IllegalArgumentException(v);
    }

}
