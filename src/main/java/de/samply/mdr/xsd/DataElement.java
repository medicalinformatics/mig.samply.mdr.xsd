
package de.samply.mdr.xsd;

import java.io.Serializable;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.adapters.CollapsedStringAdapter;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;


/**
 * <p>Java class for dataElement complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="dataElement"&gt;
 *   &lt;complexContent&gt;
 *     &lt;extension base="{http://schema.samply.de/mdr/common}element"&gt;
 *       &lt;sequence&gt;
 *         &lt;element name="valueDomain" type="{http://schema.samply.de/mdr/common}uuid"/&gt;
 *       &lt;/sequence&gt;
 *     &lt;/extension&gt;
 *   &lt;/complexContent&gt;
 * &lt;/complexType&gt;
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "dataElement", propOrder = {
    "valueDomain"
})
public class DataElement
    extends Element
    implements Serializable
{

    private final static long serialVersionUID = 1L;
    @XmlElement(required = true)
    @XmlJavaTypeAdapter(CollapsedStringAdapter.class)
    @XmlSchemaType(name = "token")
    protected String valueDomain;

    /**
     * Gets the value of the valueDomain property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValueDomain() {
        return valueDomain;
    }

    /**
     * Sets the value of the valueDomain property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValueDomain(String value) {
        this.valueDomain = value;
    }

}
